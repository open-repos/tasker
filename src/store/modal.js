export default {
    state: () => ({
        open: false,
        title: '',
        component_name: 'confirm_form',
        data_for_component: null
    }),

    mutations: {
        OPEN_MODAL(state, payload) {
            state.open = true
            state.title = payload.title
            state.component_name = payload.component_name
            state.data_for_component = payload.data_for_component
        },

        CLOSE_MODAL(state) {
            state.open = false
            state.title = ''
            state.component_name = 'confirm_form'
            state.data_for_component = null
        },
    },

    actions: {
        OPEN_MODAL({ commit }, payload) {
            commit('OPEN_MODAL', payload)
        },

        CLOSE_MODAL({ commit }) {
            commit('CLOSE_MODAL')
        },
    },

    getters: {
        modalOpen: state => {
            return state.open
        },

        modalTitle: state => {
            return state.title
        },

        modalComponentName: state => {
            return state.component_name
        },

        modalDataForComponent: state => {
            return state.data_for_component
        },
    }
}