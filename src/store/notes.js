import Vue from 'vue'

export default {
    state: () => ({
        notes: [],
    }),

    mutations: {
        ADD_NOTE(state, note) {
            state.notes.push({
                title: note.title,
                text: note.text,
                time: new Date().getTime(),
                check_list: {},
            })
        },

        EDIT_NOTE(state, updated_notes) {
            state.notes = updated_notes
        },

        ARCHIVE_TASK(state, updated_tasks) {
            state.notes = updated_tasks
        },

        DELETE_NOTE(state, updated_notes) {
            state.notes = updated_notes
        },

        ADD_ITEM_TO_CHECK_LIST(state, updated_tasks) {
            state.notes = updated_tasks
        },

        TOGGLE_CHECK_LIST_ITEM_STATE(state, updated_tasks) {
            state.notes = updated_tasks
        },

        TOGGLE_CHECK_LIST_ITEM_EDIT(state, updated_tasks) {
            state.notes = updated_tasks
        },
    },

    actions: {
        ADD_NOTE({ commit }, note_data) {
            note_data.title = note_data.title.length === 0 ? 'Без названия' : note_data.title
            commit('ADD_NOTE', note_data)
        },

        EDIT_NOTE({ commit, state }, note_data) {
            let updated_notes = state.notes.map(note => {
                if ( note.time === note_data.time ) {
                    note.title = note_data.title
                    note.text = note_data.text
                }
                return note
            })
            commit('EDIT_NOTE', updated_notes)
        },

        ARCHIVE_TASK({ commit, state }, time) {
            let updated_tasks = state.notes.map(task => {
                if ( task.time == time ) {
                    task.archive = true
                }
                return task
            })
            commit('ARCHIVE_TASK', updated_tasks)
        },

        DELETE_NOTE({ commit, state }, time) {
            let updated_notes = state.notes.filter(note => {
                return note.time != time
            })
            commit('DELETE_NOTE', updated_notes)
        },

        ADD_ITEM_TO_CHECK_LIST({ commit, state }, { time_id, item_text }) {
            let updated_tasks = state.notes.map(task => {
                if ( task.time == time_id ) {
                    let list_item_time_id = new Date().getTime()
                    Vue.set(task.check_list, list_item_time_id, {
                        completed: false,
                        text: item_text,
                        list_item_time_id: list_item_time_id,
                        edited_now: false,
                    })
                }
                return task
            })
            commit('ADD_ITEM_TO_CHECK_LIST', updated_tasks)
        },

        TOGGLE_CHECK_LIST_ITEM_STATE({ commit, state }, { time_id, list_item_time_id }) {
            let updated_tasks = state.notes.map(task => {
                if ( task.time == time_id ) {
                    task.check_list[list_item_time_id].completed = !task.check_list[list_item_time_id].completed 
                }
                return task
            })
            commit('TOGGLE_CHECK_LIST_ITEM_STATE', updated_tasks)
        },

        TOGGLE_CHECK_LIST_ITEM_EDIT({ commit, state }, { time_id, list_item_time_id }) {
            let updated_tasks = state.notes.map(task => {
                if ( task.time == time_id ) {
                    for (const key in task.check_list) {
                        if ( list_item_time_id == key ) {
                            task.check_list[key].edited_now = true
                        }
                        else {
                            task.check_list[key].edited_now = false
                        }
                    }
                }
                return task
            })
            commit('TOGGLE_CHECK_LIST_ITEM_EDIT', updated_tasks)
        },
    },

    getters: {
        getActiveTasks: state => {
            let tasks_reverse = Array.from(state.notes)
            tasks_reverse.reverse()
            return tasks_reverse.filter(task => {
                return task.archive === undefined || task.archive === false
            })
        },

        getNoteByTime: state => time =>  {
            return state.notes.filter(note => {
                return note.time == time
            })[0]
        },

        getActiveTasksCount: state =>  {
            return state.notes.filter(task => {
                return task.archive === undefined || task.archive === false
            }).length
        },

        getArchiveTasksCount: state =>  {
            return state.notes.filter(task => {
                return task.archive === true
            }).length
        },

        getCheckListItems: state => (time_id, check_list_search) =>  {
            let task = state.notes.filter(note => {
                return note.time == time_id
            })[0]

            if ( check_list_search === '' ) {
                return task.check_list
            }
            else {
                let filtred_check_list = {}
                for (const key in task.check_list) {
                    if ( new RegExp( check_list_search, 'g' ).test(task.check_list[key].text) ) {
                        filtred_check_list[key] = task.check_list[key]
                    }
                }
                return filtred_check_list
            }
        },
    }
}