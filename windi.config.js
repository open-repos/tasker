import { defineConfig } from 'windicss/helpers'

export default defineConfig({
    theme: {
        extend: {
            screens: {
            },
            colors: {
                clr: {
                    'blue': '#08A0F7',
                    'dim-blue': '#0891b2',
                    'green': '#34A770',
                    'green-light': '#8bc34a',
                    'yellow': '#FBA63C',
                    'red': '#F85640',
                    'gray': '#F4F7FC',
                    'dark': '#121212',
                    'white': '#fff',
                }
            },
            fontFamily: {
                sans: ['Open Sans', 'sans-serif'],
            },
        },
    },
})